var download_pages = module.exports;

var http = require("http");
var fs = require("fs");

function download(url, callback) {
  http.get(url, function(res) {
    var data = "";
    res.on('data', function (chunk) {
        data += chunk;
    });
    res.on("end", function() {
        callback(data);
    });
  }).on("error", function(e) {
        callback(e);
  });
}

download_pages.getResponse = function(url, callback) {
    download(url, callback);
};